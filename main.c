#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/un.h>

#include <libdrm/amdgpu.h>

#include <drm/drm.h>
#include <libdrm/amdgpu_drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

#include <assert.h>

#define MAX_CARDS_SUPPORTED     128

#define GFX_COMPUTE_NOP  0xffff1000
#define GFX_COMPUTE_NOP_SI 0x80000000

#define	PACKET_TYPE3	3

#define PACKET3(op, n)	((PACKET_TYPE3 << 30) |				\
			 (((op) & 0xFF) << 8) |				\
			 ((n) & 0x3FFF) << 16)
#define PACKET3_COMPUTE(op, n) PACKET3(op, n) | (1 << 1)

/* Packet 3 types */
#define	PACKET3_NOP					0x10
#define PACKET3_COPY_DATA                         0x40
#define		COUNT_SEL                             (1 << 16)

#define	PACKET3_WRITE_DATA				0x37
#define		WRITE_DATA_DST_SEL(x)                   ((x) << 8)
		/* 0 - register
		 * 1 - memory (sync - via GRBM)
		 * 2 - gl2
		 * 3 - gds
		 * 4 - reserved
		 * 5 - memory (async - direct)
		 */
#define		WR_ONE_ADDR                             (1 << 16)
#define		WR_CONFIRM                              (1 << 20)
#define		WRITE_DATA_CACHE_POLICY(x)              ((x) << 25)
		/* 0 - LRU
		 * 1 - Stream
		 */
#define		WRITE_DATA_ENGINE_SEL(x)                ((x) << 30)
		/* 0 - me
		 * 1 - pfp
		 * 2 - ce
		 */


#define SWAP_32(num) (((num & 0xff000000) >> 24) | \
		      ((num & 0x0000ff00) << 8) | \
		      ((num & 0x00ff0000) >> 8) | \
		      ((num & 0x000000ff) << 24))


static  uint32_t shader_bin[] = {
	SWAP_32(0x800082be), SWAP_32(0x02ff08bf), SWAP_32(0x7f969800), SWAP_32(0x040085bf),
	SWAP_32(0x02810281), SWAP_32(0x02ff08bf), SWAP_32(0x7f969800), SWAP_32(0xfcff84bf),
	SWAP_32(0xff0083be), SWAP_32(0x00f00000), SWAP_32(0xc10082be), SWAP_32(0xaa02007e),
	SWAP_32(0x000070e0), SWAP_32(0x00000080), SWAP_32(0x000081bf)
};

#define CODE_OFFSET 512
#define DATA_OFFSET 1024

#define PKT3_SET_SH_REG                        0x76
#define		PACKET3_SET_SH_REG_START			0x00002c00

#define	PACKET3_DISPATCH_DIRECT				0x15
#define mmCOMPUTE_PGM_LO                                                        0x2e0c
#define mmCOMPUTE_PGM_RSRC1                                                     0x2e12
#define mmCOMPUTE_TMPRING_SIZE                                                  0x2e18
#define mmCOMPUTE_USER_DATA_0                                                   0x2e40
#define mmCOMPUTE_USER_DATA_1                                                   0x2e41
#define mmCOMPUTE_RESOURCE_LIMITS                                               0x2e15
#define mmCOMPUTE_NUM_THREAD_X                                                  0x2e07

#define PKT3_CONTEXT_CONTROL                   0x28
#define     CONTEXT_CONTROL_LOAD_ENABLE(x)     (((unsigned)(x) & 0x1) << 31)
#define     CONTEXT_CONTROL_LOAD_CE_RAM(x)     (((unsigned)(x) & 0x1) << 28)
#define     CONTEXT_CONTROL_SHADOW_ENABLE(x)   (((unsigned)(x) & 0x1) << 31)

#define PKT3_CLEAR_STATE                       0x12


int drm_amdgpu[MAX_CARDS_SUPPORTED];

static amdgpu_device_handle device_handle;
static uint32_t major_version;
static uint32_t minor_version;


amdgpu_bo_handle ib_handle;
void *ib_cpu;
uint64_t ib_mc_address;

struct amdgpu_cs_request ibs_request;
struct amdgpu_cs_ib_info ib_info;
amdgpu_bo_list_handle bo_list;
amdgpu_va_handle va_handle[3];
struct amdgpu_cs_fence fence_status = {0};
// struct drm_amdgpu_info_hw_ip info;

int i, j, r;
int write_length = 128;
uint32_t *ptr, sync_obj_handle, sync_obj_handle2;

amdgpu_context_handle context_handle;

amdgpu_bo_handle bo;
uint64_t bo_mc;
volatile uint32_t *bo_cpu;

uint64_t shader_dst;

amdgpu_bo_handle resources[3];

uint64_t imported_bo_va = 0;

uint32_t *pm4;
const int pm4_dw = 256;

uint32_t expired;

#define IB_SIZE		4096

static int amdgpu_open_devices(int open_render_node)
{
	drmDevicePtr devices[MAX_CARDS_SUPPORTED];
	int i;
	int drm_node;
	int amd_index = 0;
	int drm_count;
	int fd;
	drmVersionPtr version;

	drm_count = drmGetDevices2(0, devices, MAX_CARDS_SUPPORTED);

	if (drm_count < 0) {
		fprintf(stderr,
			"drmGetDevices2() returned an error %d\n",
			drm_count);
		return 0;
	}

	for (i = 0; i < drm_count; i++) {
		/* If this is not PCI device, skip*/
		if (devices[i]->bustype != DRM_BUS_PCI)
			continue;

		/* If this is not AMD GPU vender ID, skip*/
		if (devices[i]->deviceinfo.pci->vendor_id != 0x1002)
			continue;

		if (open_render_node)
			drm_node = DRM_NODE_RENDER;
		else
			drm_node = DRM_NODE_PRIMARY;

		fd = -1;
		if (devices[i]->available_nodes & 1 << drm_node)
			fd = open(
				devices[i]->nodes[drm_node],
				O_RDWR | O_CLOEXEC);

		/* This node is not available. */
		if (fd < 0) continue;

		version = drmGetVersion(fd);
		if (!version) {
			fprintf(stderr,
				"Warning: Cannot get version for %s."
				"Error is %s\n",
				devices[i]->nodes[drm_node],
				strerror(errno));
			close(fd);
			continue;
		}

		if (strcmp(version->name, "amdgpu")) {
			/* This is not AMDGPU driver, skip.*/
			drmFreeVersion(version);
			close(fd);
			continue;
		}

		drmFreeVersion(version);

		drm_amdgpu[amd_index] = fd;
		amd_index++;
	}

	drmFreeDevices(devices, drm_count);
	return amd_index;
}

/* Close AMD devices.
 */
void amdgpu_close_devices()
{
	int i;
	for (i = 0; i < MAX_CARDS_SUPPORTED; i++)
		if (drm_amdgpu[i] >=0) {
			close(drm_amdgpu[i]);
		}
}

#define CU_ASSERT_EQUAL(actual, expected) \
  { assert(actual == expected); }

#define __align_mask(value, mask)  (((value) + (mask)) & ~(mask))
#define ALIGN(value, alignment)    __align_mask(value, (__typeof__(value))((alignment) - 1))

int amdgpu_bo_alloc_and_map_raw(amdgpu_device_handle dev, unsigned size,
			unsigned alignment, unsigned heap, uint64_t alloc_flags,
			uint64_t mapping_flags, amdgpu_bo_handle *bo, void **cpu,
			uint64_t *mc_address,
			amdgpu_va_handle *va_handle)
{
	struct amdgpu_bo_alloc_request request = {};
	amdgpu_bo_handle buf_handle;
	amdgpu_va_handle handle;
	uint64_t vmc_addr;
	int r;

	request.alloc_size = size;
	request.phys_alignment = alignment;
	request.preferred_heap = heap;
	request.flags = alloc_flags;

	r = amdgpu_bo_alloc(dev, &request, &buf_handle);
	if (r)
		return r;

	r = amdgpu_va_range_alloc(dev,
				  amdgpu_gpu_va_range_general,
				  size, alignment, 0, &vmc_addr,
				  &handle, 0);
	if (r)
		goto error_va_alloc;

	r = amdgpu_bo_va_op_raw(dev, buf_handle, 0,  ALIGN(size, getpagesize()), vmc_addr,
				   AMDGPU_VM_PAGE_READABLE |
				   AMDGPU_VM_PAGE_WRITEABLE |
				   AMDGPU_VM_PAGE_EXECUTABLE |
				   mapping_flags,
				   AMDGPU_VA_OP_MAP);
	if (r)
		goto error_va_map;

	r = amdgpu_bo_cpu_map(buf_handle, cpu);
	if (r)
		goto error_cpu_map;

	*bo = buf_handle;
	*mc_address = vmc_addr;
	*va_handle = handle;

	return 0;

 error_cpu_map:
	amdgpu_bo_cpu_unmap(buf_handle);

 error_va_map:
	amdgpu_bo_va_op(buf_handle, 0, size, vmc_addr, 0, AMDGPU_VA_OP_UNMAP);

 error_va_alloc:
	amdgpu_bo_free(buf_handle);
	return r;
}


static inline int
amdgpu_bo_alloc_and_map(amdgpu_device_handle dev, unsigned size,
			unsigned alignment, unsigned heap, uint64_t alloc_flags,
			amdgpu_bo_handle *bo, void **cpu, uint64_t *mc_address,
			amdgpu_va_handle *va_handle)
{
	return amdgpu_bo_alloc_and_map_raw(dev, size, alignment, heap,
					alloc_flags, 0, bo, cpu, mc_address, va_handle);
}


static inline int
amdgpu_bo_unmap_and_free(amdgpu_bo_handle bo, amdgpu_va_handle va_handle,
			 uint64_t mc_addr, uint64_t size)
{
	amdgpu_bo_cpu_unmap(bo);
	amdgpu_bo_va_op(bo, 0, size, mc_addr, 0, AMDGPU_VA_OP_UNMAP);
	amdgpu_va_range_free(va_handle);
	amdgpu_bo_free(bo);

	return 0;

}

// char *myfifo = "/tmp/myfifo";
char *socket_path = "/tmp/amdgpu_test.socket";

void send_fd(int socket, int *fds, int n)  // send fd by socket
{
        struct msghdr msg = {0};
        struct cmsghdr *cmsg;
        char buf[CMSG_SPACE(n * sizeof(int))], dup[256];
        memset(buf, '\0', sizeof(buf));
        struct iovec io = { .iov_base = &dup, .iov_len = sizeof(dup) };

        msg.msg_iov = &io;
        msg.msg_iovlen = 1;
        msg.msg_control = buf;
        msg.msg_controllen = sizeof(buf);

        cmsg = CMSG_FIRSTHDR(&msg);
        cmsg->cmsg_level = SOL_SOCKET;
        cmsg->cmsg_type = SCM_RIGHTS;
        cmsg->cmsg_len = CMSG_LEN(n * sizeof(int));

        memcpy ((int *) CMSG_DATA(cmsg), fds, n * sizeof (int));

        int ret;
        ret = sendmsg (socket, &msg, 0);
        if (ret < 0)
            assert(0);
//                handle_error (“Failed to send message”);
}

int * recv_fd(int socket, int n) {
        int *fds = malloc (n * sizeof(int));
        struct msghdr msg = {0};
        struct cmsghdr *cmsg;
        char buf[CMSG_SPACE(n * sizeof(int))], dup[256];
        memset(buf, '\0', sizeof(buf));
        struct iovec io = { .iov_base = &dup, .iov_len = sizeof(dup) };

        msg.msg_iov = &io;
        msg.msg_iovlen = 1;
        msg.msg_control = buf;
        msg.msg_controllen = sizeof(buf);

        int ret;
        ret = recvmsg (socket, &msg, 0);
        if (ret < 0)
            assert(0);
//                handle_error (“Failed to receive message”);

        cmsg = CMSG_FIRSTHDR(&msg);

        memcpy (fds, (int *) CMSG_DATA(cmsg), n * sizeof(int));

        return fds;
}

void main(int argc, char *argv[]) {
    int fd, cfd;
    char engine;

    int open_render_node = 0;



    if (argc < 2)
        return;

    struct sockaddr_un addr;
    fd = socket(AF_UNIX, SOCK_STREAM, 0);

    // if (engine == 'g')
    //     unlink(socket_path);
    // else
    //     sleep(3);

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);


    engine = argv[1][0];

    if (engine == 'g')
        printf("gfx\n");
    else if (engine == 'c')
        printf("compute\n");
    else
        return;


    // if (engine == 'c')
    //     open_render_node = 1;

	for (i = 0; i < MAX_CARDS_SUPPORTED; i++)
		drm_amdgpu[i] = -1;

	if (amdgpu_open_devices(open_render_node) <= 0) {
		printf("Cannot open AMDGPU device\n");
        return;
	}

	if (drm_amdgpu[0] < 0) {
		printf("Cannot open AMDGPU device\n");
        return;
	}

	r = amdgpu_device_initialize(drm_amdgpu[0], &major_version,
					&minor_version, &device_handle);

    assert(device_handle > 0);

	printf("%c: device handle: %x\n", engine, device_handle);
    fflush(stdout);

    // return;


	// create context
	r = amdgpu_cs_ctx_create(device_handle, &context_handle);
	CU_ASSERT_EQUAL(r, 0);

	// IB memory
	r = amdgpu_bo_alloc_and_map(device_handle, 8192, 4096,
		AMDGPU_GEM_DOMAIN_GTT, 0,
		&ib_handle, &ib_cpu,
		&ib_mc_address, &va_handle[0]);
	CU_ASSERT_EQUAL(r, 0);

	// test buffer
	r = amdgpu_bo_alloc_and_map(device_handle, 4096, 4096,
		AMDGPU_GEM_DOMAIN_GTT, 0,
		&bo, &bo_cpu,
		&bo_mc, &va_handle[1]);
	CU_ASSERT_EQUAL(r, 0);

	uint32_t shared_handle = 0;
	struct amdgpu_bo_import_result res = {0};

    char buffer[32];

	if (engine == 'g') {
		// gpu - export
		r = amdgpu_bo_export(bo, amdgpu_bo_handle_type_dma_buf_fd, &shared_handle);  // amdgpu_bo_handle_type_dma_buf_fd
        snprintf(buffer, 32, "%x", 0xbeefbeef);

        uint32_t fds[3] = {shared_handle, 0xff, 0xff};

        unlink(socket_path);
        r = bind(fd, (struct sockaddr*)&addr, sizeof(addr));
        CU_ASSERT_EQUAL(r, 0);
        r = listen(fd, 5);
        CU_ASSERT_EQUAL(r, 0);
        cfd = accept(fd, NULL, NULL);
        assert(cfd > 0);
        // sleep(5);

        // r = send(cfd, buffer, strlen(buffer), 0);
        send_fd(cfd, fds, 1);
        //write(fd, &shared_handle, sizeof(shared_handle));
        // write(fd, buffer, strlen(buffer) + 1);
        // read(fd, buffer, sizeof(buffer));
        // close(cl);
        // close(fd);

		printf("exported %x", shared_handle);
                    fflush(stdout);

		// CU_ASSERT_EQUAL(r, 0);
	}
	else {
		// compute - import
        // sleep(5);
        r = -1;
        while (r < 0)
            r = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
        CU_ASSERT_EQUAL(r, 0);
        int len = 0;
        int *shared_handles;

        // r = recv(fd, buffer, 3, 0);
        shared_handles = recv_fd(fd, 1);
        shared_handle = shared_handles[0];
        // while ((len = read(fd, buffer, sizeof(buffer))) > 0);
        // len = read(fd, buffer, sizeof(buffer));
        // close(fd);
        // shared_handle = 2; //sscanf(buffer, "%x");
		printf("importing %x", shared_handle);
            fflush(stdout);

        r = amdgpu_bo_import(device_handle, amdgpu_bo_handle_type_dma_buf_fd, shared_handle, &res);
		CU_ASSERT_EQUAL(r, 0);
		r = amdgpu_va_range_alloc(device_handle,
					amdgpu_gpu_va_range_general,
					4096, 1, 0, &imported_bo_va,
					&va_handle[2], 0);
		CU_ASSERT_EQUAL(r, 0);
		r = amdgpu_bo_va_op(res.buf_handle, 0, 4096, imported_bo_va, 0, AMDGPU_VA_OP_MAP);
		CU_ASSERT_EQUAL(r, 0);
	}

    // sleep(5);

	if (engine == 'g') {
		// cpu - fill memory with value A
		ptr = bo_cpu;
		for (j = 0; j < write_length; ++j)
			ptr[j] = 0xdeaddead;
	}

	// copy shader code to video memory (IB + offset)
	ptr = ib_cpu;
	memcpy(ptr + CODE_OFFSET , shader_bin, sizeof(shader_bin));

	j = i = 0;
	pm4 = ib_cpu;

	// long shader task
	if (engine == 'g') {
		/* Dispatch minimal init config and verify it's executed */
		pm4[i++] = PACKET3(PKT3_CONTEXT_CONTROL, 1);
		pm4[i++] = 0x80000000;
		pm4[i++] = 0x80000000;

		pm4[i++] = PACKET3(PKT3_CLEAR_STATE, 0);
		pm4[i++] = 0x80000000;
	}

	/* Program compute regs */
	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 2);
	pm4[i++] = mmCOMPUTE_PGM_LO - PACKET3_SET_SH_REG_START;
	pm4[i++] = (ib_mc_address + CODE_OFFSET * 4) >> 8;
	pm4[i++] = (ib_mc_address + CODE_OFFSET * 4) >> 40;


	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 2);
	pm4[i++] = mmCOMPUTE_PGM_RSRC1 - PACKET3_SET_SH_REG_START;
	/*
	 * 002c0040         COMPUTE_PGM_RSRC1 <- VGPRS = 0
	                                      SGPRS = 1
	                                      PRIORITY = 0
	                                      FLOAT_MODE = 192 (0xc0)
	                                      PRIV = 0
	                                      DX10_CLAMP = 1
	                                      DEBUG_MODE = 0
	                                      IEEE_MODE = 0
	                                      BULKY = 0
	                                      CDBG_USER = 0
	 *
	 */
	pm4[i++] = 0x002c0040;


	/*
	 * 00000010         COMPUTE_PGM_RSRC2 <- SCRATCH_EN = 0
	                                      USER_SGPR = 8
	                                      TRAP_PRESENT = 0
	                                      TGID_X_EN = 0
	                                      TGID_Y_EN = 0
	                                      TGID_Z_EN = 0
	                                      TG_SIZE_EN = 0
	                                      TIDIG_COMP_CNT = 0
	                                      EXCP_EN_MSB = 0
	                                      LDS_SIZE = 0
	                                      EXCP_EN = 0
	 *
	 */
	pm4[i++] = 0x00000010;


/*
 * 00000100         COMPUTE_TMPRING_SIZE <- WAVES = 256 (0x100)
                                         WAVESIZE = 0
 *
 */
	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 1);
	pm4[i++] = mmCOMPUTE_TMPRING_SIZE - PACKET3_SET_SH_REG_START;
	pm4[i++] = 0x00000100;

	if (engine == 'g') {	/* gfx */
		shader_dst = bo_mc;
	}
	else {	    /* compute */
		shader_dst = ib_mc_address + DATA_OFFSET * 4;
	}

	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 2);
	pm4[i++] = mmCOMPUTE_USER_DATA_0 - PACKET3_SET_SH_REG_START;
	pm4[i++] = 0xffffffff & shader_dst;
	pm4[i++] = (0xffffffff00000000 & shader_dst) >> 32;

	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 1);
	pm4[i++] = mmCOMPUTE_RESOURCE_LIMITS - PACKET3_SET_SH_REG_START;
	pm4[i++] = 0;

	pm4[i++] = PACKET3(PKT3_SET_SH_REG, 3);
	pm4[i++] = mmCOMPUTE_NUM_THREAD_X - PACKET3_SET_SH_REG_START;
	pm4[i++] = 1;
	pm4[i++] = 1;
	pm4[i++] = 1;

    for (int x = 0; x < 10; x++) {
        /* Dispatch */
        pm4[i++] = PACKET3(PACKET3_DISPATCH_DIRECT, 3);
        pm4[i++] = 1;
        pm4[i++] = 1;
        pm4[i++] = 1;
        pm4[i++] = 0x00000045; /* DISPATCH DIRECT field */
    }


	while (i & 7)
		pm4[i++] =  0xffff1000; /* type3 nop packet */

	if (engine == 'c') {
		// compute - copy memory to new memory
		pm4[i++] = PACKET3_COMPUTE(PACKET3_COPY_DATA, 4);
		pm4[i++] = WRITE_DATA_DST_SEL(5) | COUNT_SEL | WR_CONFIRM | 1;
		pm4[i++] = 0xfffffffc & imported_bo_va;
		pm4[i++] = (0xffffffff00000000 & imported_bo_va) >> 32;
		pm4[i++] = 0xfffffffc & bo_mc;
		pm4[i++] = (0xffffffff00000000 & bo_mc) >> 32;

		while (i & 7)
			pm4[i++] = 0xffff1000; //PACKET3(PACKET3_NOP, 14); //GFX_COMPUTE_NOP_SI; /* type3 nop packet */
	}
	memset(&ib_info, 0, sizeof(struct amdgpu_cs_ib_info));
	ib_info.ib_mc_address = ib_mc_address; // + j * 4;
	ib_info.size = i; // - j;


	// submit IBs
	if (engine == 'g') {
		resources[0] = ib_handle;
		resources[1] = bo;
		r = amdgpu_bo_list_create(device_handle, 2, resources, NULL, &bo_list);
	}
	else {
		resources[0] = ib_handle;
		resources[1] = bo;
		resources[2] = res.buf_handle;
		r = amdgpu_bo_list_create(device_handle, 3, resources, NULL, &bo_list);
	}

	int seq_no;

	memset(&ibs_request, 0, sizeof(struct amdgpu_cs_request));

	if (engine == 'g') {
		ibs_request.ip_type = AMDGPU_HW_IP_GFX;
		ibs_request.ring = 0;
		ibs_request.number_of_ibs = 1;
		ibs_request.ibs = &ib_info;
		ibs_request.resources = bo_list;
		CU_ASSERT_EQUAL(amdgpu_cs_submit(context_handle, 0, &ibs_request, 1), 0);

		seq_no = ibs_request.seq_no;

		fence_status.context = context_handle;
		fence_status.ip_type = AMDGPU_HW_IP_GFX;
		fence_status.ip_instance = 0;
		fence_status.ring = 0;
		fence_status.fence = seq_no;
	}
	else {
		ibs_request.ip_type = AMDGPU_HW_IP_COMPUTE;
		ibs_request.ring = 0;
		ibs_request.number_of_ibs = 1;
		ibs_request.ibs = &ib_info;
		ibs_request.resources = bo_list;
		ibs_request.fence_info.handle = NULL;
		CU_ASSERT_EQUAL(amdgpu_cs_submit(context_handle, 0, &ibs_request, 1), 0);

		seq_no = ibs_request.seq_no;

		fence_status.context = context_handle;
		fence_status.ip_type = AMDGPU_HW_IP_COMPUTE;
		fence_status.ip_instance = 0;
		fence_status.ring = 0;
		fence_status.fence = seq_no;
	}

	r = amdgpu_cs_query_fence_status(&fence_status,
					 AMDGPU_TIMEOUT_INFINITE,
					 0, &expired);


	if (engine == 'c') {
        assert(bo_cpu[0] == 42);
	// 	// compute - new memory should contain value B
	// 	i = 0;
	// 	while (++i < 2) {
    //         if (((int *)bo_cpu)[i] != 0xbeefbeef) {
    //             printf("we have a problem...\n");
    //             return;
    //         }
	// 		// CU_ASSERT_EQUAL(((int*)bo_cpu)[i], 0xbeefbeef);
	// 	}
	}

    // sleep(5);

	// clean up
	if (engine == 'c') {
		r = amdgpu_bo_va_op(res.buf_handle, 0, 4096, imported_bo_va, 0, AMDGPU_VA_OP_UNMAP);
		CU_ASSERT_EQUAL(r, 0);

		r = amdgpu_va_range_free(va_handle[2]);
		CU_ASSERT_EQUAL(r, 0);

		r = amdgpu_bo_free(res.buf_handle);
		CU_ASSERT_EQUAL(r, 0);
	}

	r = amdgpu_bo_unmap_and_free(ib_handle, va_handle[0],
		ib_mc_address, IB_SIZE);
	CU_ASSERT_EQUAL(r, 0);

	r = amdgpu_bo_unmap_and_free(bo, va_handle[1],
		bo_mc, IB_SIZE);
	CU_ASSERT_EQUAL(r, 0);

	r = amdgpu_cs_ctx_free(context_handle);
	CU_ASSERT_EQUAL(r, 0);





    amdgpu_device_deinitialize(device_handle);

    printf("Done!\n");

}